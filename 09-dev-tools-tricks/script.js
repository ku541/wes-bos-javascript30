const dogs = [
    {
        name: 'Snickers',
        age: 2
    },
    {
        name: 'Hugo',
        age: 8
    }
];
const p = document.querySelector('p');

function makeGreen() {
    p.style.color = '#BADA55';
    p.style.fontSize = '50px';
}

// Regular
console.log('hello');

// Interpolated
console.log('Hello I am a %s string!', ':)');
// console.log(`Hello I am ${var}`);

// Styled
console.log('%c I am some great text', 'font-size: 50px; background: red; text-shadow: 10px 10px 0 blue');

// Warning!
console.warn('WATCH IT -_-');

// Error!!!
console.error('SHIT T_T');

// Info
console.info('Cats Do Not Taste Sweet, But Dogs Taste Salty.');

// Testing
console.assert(p.classList.contains('ouch'), 'That is wrong!');

// Clear
console.clear();

// Viewing DOM Elements
console.dir(p);

// Grouping Together
dogs.forEach(dog => {
    console.group(`${dog.name}`);
    // console.groupCollapsed(`${dog.name}`);
    console.log(`This is ${dog.name}`);
    console.log(`${dog.name} is ${dog.age} old`);
    console.log(`${dog.name} is ${dog.age * 7} dog years old`);
    console.groupEnd(`${dog.name}`);
});

// Counting
console.count('Wes');
console.count('Wes');
console.count('Steve');
console.count('Steve');
console.count('Wes');
console.count('Steve');
console.count('Wes');
console.count('Steve');
console.count('Steve');
console.count('Steve');
console.count('Steve');
console.count('Steve');

// Timing
console.time('Fetching Data');
fetch('https://api.github.com/users/wesbos')
    .then(data => data.json())
    .then(data => {
        console.timeEnd('Fetching Data');
        console.log(data);
    });

// Table
console.table(dogs);