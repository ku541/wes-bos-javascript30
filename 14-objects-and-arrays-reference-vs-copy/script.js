// Start with strings, numbers and booleans.
let age = 100;
let age2 = age;
let name = 'Bos';
let name2 = name;

console.log(age, age2);
console.log(name, name2);

age = 200;
name = 'Wes';

console.log(age, age2);
console.log(name, name2);

// Let's say we have an array,
const players = ['Wes', 'Sarah', 'Ryan', 'Poppy'];

// and we want to make a copy of it.
const team = players;

console.log(team, players);

// You might think that we can just do something like this:
team[3] = 'Lux';

// However what happens when we update that array?
console.log(team);

// Now here is the problem!
console.log(players);

// Oh no!, we have edited the original array too!, why?
// It's because that is an array reference, not an array
// copy. They both point to the same array!.

// So, how do we fix this? We take a copy instead using slice!.
const team2 = players.slice();

// Or create a new array and concat the old one in.
const team3 = [].concat(players);

// Or use the new ES6 spread.
const team4 = [...players];

// Or use Array.from().
const team5 = Array.from(players);

// No when we update it, the original one isn't chnged.
console.log(players, team5);

team5[3] = 'Flux';

console.log(players, team5);

// The same thing goes for objects, let's say we have a person object
const person = {
    name: 'Wes Bos',
    age: 80
}

// and think we make a copy:
const captain = person;

captain.age = 99;

console.log(captain, person);

// How do we make a copy instead?
const captain2 = Object.assign({}, person, { age: 100 });

console.log(captain2, person);

// Things to note.
// 1. This is only one level deep for arrays and objects.
// 2. Lodash has a cloneDeep method,
// 3. but you should think twice before using it.

const wes = {
    name: 'Wes',
    age: 100,
    social: {
        twitter: '@wesbos',
        facebook: 'wesbos.developer'
    }
}

const dev = Object.assign({}, wes);

dev.social.twitter = "@WesDaBoss";

console.log(wes, dev);

// Things to note.
// 1. Hack! - Poor Man's Deep Clone
// 2. 99 shakes all the references and returns a full copy of the given object.
// 3. Research performance before using.

const dev2 = JSON.parse(JSON.stringify(wes));

dev2.social.twitter = "@WesDaBoss";

console.log(wes, dev2);