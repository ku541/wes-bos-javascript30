const bands = [
    'The Bled',
    'Norma Jean',
    'An Old Dog',
    'Oh, Sleeper',
    'Say Anything',
    'Counterparts',
    'A Skylit Drive',
    'The Plot in You',
    'Pierce the Veil',
    'The Midway State',
    'We Came as Romans',
    'Anywhere But Here',
    'The Devil Wears Prada'
];

function strip(bandName) {
    return bandName.replace(/^(a |the |an)/i, '').trim();
}

const sortedBands = bands.sort((a, b) => strip(a) > strip(b) ? 1 : -1);

document.querySelector('#bands').innerHTML = 
    sortedBands.map(band => `<li>${band}</li>`)
               .join('');