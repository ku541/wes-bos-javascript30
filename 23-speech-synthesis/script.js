const speechRequest = new SpeechSynthesisUtterance;
const stopButton = document.querySelector('#stop');
const speakButton = document.querySelector('#speak');
const voicesDropdown = document.querySelector('[name="voice"]');
const options = document.querySelectorAll('[type="range"], [name="text"]');

let voices = [];

speechRequest.text = document.querySelector('[name="text"]').value;

speakButton.addEventListener('click', toggle);
stopButton.addEventListener('click', () => toggle(false));
voicesDropdown.addEventListener('change', setVoice);
speechSynthesis.addEventListener('voiceschanged', populateVoices);
options.forEach(option => option.addEventListener('change', setOption));

function populateVoices() {
    voices = this.getVoices();

    voicesDropdown.innerHTML = voices
        .map(voice => `<option value="${voice.name}">${voice.name} (${voice.lang})</option>`)
        .join('');
}

function setVoice() {
    speechRequest.voice = voices.find(voice => voice.name === this.value);

    toggle();
}

function toggle(startOver = true) {
    speechSynthesis.cancel();

    if (startOver) {
        speechSynthesis.speak(speechRequest);
    }
}

function setOption() {
    speechRequest[this.name] = this.value;

    toggle();
}