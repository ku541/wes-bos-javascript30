let countdown;
const buttons = document.querySelectorAll('[data-time]');
const endTime = document.querySelector('.display__end-time');
const timerDisplay = document.querySelector('.display__time-left');

function timer(seconds) {
    clearInterval(countdown);

    const now = Date.now();
    const then = now + seconds * 1000;

    displayTimeLeft(seconds);
    displayEndTime(then);

    countdown = setInterval(() => {
        const secondsLeft = Math.round((then - Date.now()) / 1000);

        if (secondsLeft <= 0) {
            return clearInterval(countdown);
        }

        displayTimeLeft(secondsLeft);
    }, 1000);
}

function displayTimeLeft(seconds) {
    const minutes = Math.floor(seconds / 60);
    const remainderSeconds = seconds % 60;
    const paddingForSeconds = remainderSeconds < 10 ? '0' : '';
    const display = `${minutes}:${paddingForSeconds}${remainderSeconds}`;

    timerDisplay.textContent = display;
    document.title = display;
}

function displayEndTime(timestamp) {
    const end = new Date(timestamp);
    const hour = end.getHours();
    const minutes = end.getMinutes();
    const adjustedHour = hour > 12 ? hour - 12 : hour;
    const paddingForinutes = minutes < 10 ? '0' : '';

    endTime.textContent = `Be Back At ${adjustedHour}:${paddingForinutes}${minutes}`;
}

function startTimer() {
    const seconds = parseInt(this.dataset.time);

    timer(seconds);
}

buttons.forEach(button => button.addEventListener('click', startTimer));
document.customForm.addEventListener('submit', function (event) {
    event.preventDefault();

    const minutes = this.minutes.value;

    timer(minutes * 60);

    this.reset();
});