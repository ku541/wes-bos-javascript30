let lastHole;
let score = 0;
let timeUp = false;
const holes = document.querySelectorAll('.hole');
const moles = document.querySelectorAll('.mole');
const scoreBoard = document.querySelector('.score');

function randomTime(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

function randomHole(holes) {
    const idx = Math.floor(Math.random() *  holes.length);
    const hole = holes[idx];

    if (hole === lastHole) {
        return randomHole(holes);
    }

    lastHole = hole;

    return hole;
}

function peep() {
    const hole = randomHole(holes);
    const time = randomTime(200, 1000);

    hole.classList.add('up');

    setTimeout(() => {
        hole.classList.remove('up');

        if (! timeUp) peep();
    }, time);
}

function startGame() {
    score = 0;
    timeUp = false;
    scoreBoard.textContent = 0;

    peep();

    setTimeout(() => timeUp = true, 10000);
}

function bonk(event) {
    if (! event.isTrusted) return;

    score++;

    this.classList.remove('up');

    scoreBoard.textContent = score;
}

moles.forEach(mole => mole.addEventListener('click', bonk));